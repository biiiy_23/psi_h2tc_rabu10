/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author HP
 */
@Entity
@Table(name="master_rt")
public class Rt {
    
    @Id
    @Column(name="id_petugas")
    @GeneratedValue
    private int id_petugas;
    
    @Column(name="nama_petugas")
    private String nama_petugas;
    
    @Column(name="no_rt")
    private String no_rt;

    public Rt() {
    }

    public Rt(int id_petugas, String nama_petugas, String no_rt) {
        this.id_petugas = id_petugas;
        this.nama_petugas = nama_petugas;
        this.no_rt = no_rt;
    }

    public int getId_petugas() {
        return id_petugas;
    }

    public void setId_petugas(int id_petugas) {
        this.id_petugas = id_petugas;
    }

    public String getNama_petugas() {
        return nama_petugas;
    }

    public void setNama_petugas(String nama_petugas) {
        this.nama_petugas = nama_petugas;
    }

    public String getNo_rt() {
        return no_rt;
    }

    public void setNo_rt(String no_rt) {
        this.no_rt = no_rt;
    }
    
}
