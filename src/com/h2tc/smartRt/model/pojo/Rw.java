/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author HP
 */
@Entity
@Table(name="master_rw")
public class Rw {
    @Id
    @Column(name="id_petugas")
    @GeneratedValue
    private int id_petugas;
    
    @Column(name="nama_petugas")
    private String nama_petugas;
    
    @Column(name="no_rw")
    private String no_rw;

    public Rw() {
    }

    public Rw(int id_petugas, String nama_petugas, String no_rw) {
        this.id_petugas = id_petugas;
        this.nama_petugas = nama_petugas;
        this.no_rw = no_rw;
    }

    public int getId_petugas() {
        return id_petugas;
    }

    public void setId_petugas(int id_petugas) {
        this.id_petugas = id_petugas;
    }

    public String getNama_petugas() {
        return nama_petugas;
    }

    public void setNama_petugas(String nama_petugas) {
        this.nama_petugas = nama_petugas;
    }

    public String getNo_rw() {
        return no_rw;
    }

    public void setNo_rw(String no_rw) {
        this.no_rw = no_rw;
    }
    
}
