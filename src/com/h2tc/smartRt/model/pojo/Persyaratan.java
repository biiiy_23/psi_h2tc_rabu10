/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.pojo;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author HP
 */
@Entity
@Table(name="persyaratan")
public class Persyaratan {
    
    @Id @GeneratedValue
    @JoinTable(name = "surat_pengantar", joinColumns = @JoinColumn(name = "id_persyaratan"),
            inverseJoinColumns = @JoinColumn(name = "id_persyaratan"))
    private String id_persyaratan;
    
    @Column(name="ktp")
    private String ktp;
    
    @Column(name="kk")
    private String kk;
    
    @Column(name = "status_pengecekan")
    private int status_pengecekan;

    public Persyaratan() {
    }

    public Persyaratan(String id_persyaratan, String ktp, String kk, int status_pengecekan) {
        this.id_persyaratan = id_persyaratan;
        this.ktp = ktp;
        this.kk = kk;
        this.status_pengecekan = status_pengecekan;
    }
    
    public Persyaratan(String ktp, String kk, int status_pengecekan) {
        this.ktp = ktp;
        this.kk = kk;
        this.status_pengecekan = status_pengecekan;
    }
    
    public String getId_persyaratan() {
        return id_persyaratan;
    }

    public void setId_persyaratan(String id_persyaratan) {
        this.id_persyaratan = id_persyaratan;
    }

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public int getStatus_pengecekan() {
        return status_pengecekan;
    }

    public void setStatus_pengecekan(int status_pengecekan) {
        this.status_pengecekan = status_pengecekan;
    }
}
