/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author HP
 */
@Entity
@Table(name="user")
public class User {
    
    @Id
    @Column(name="username", unique = true)
    @GeneratedValue
    private String username;
    
    @Column(name="password")
    private String password;
    
    @Column(name = "user_level")
    private String user_level;

    public User(String username, String password, String userLevel) {
        this.username = username;
        this.password = password;
        this.user_level = userLevel;
    }

    public User() {
    }

    public String getUserLevel() {
        return user_level;
    }

    public void setUserLevel(String userLevel) {
        this.user_level = userLevel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
