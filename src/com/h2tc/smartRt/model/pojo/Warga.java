/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

/**
 *
 * @author HP
 */
@Entity
@Table(name="master_warga")
public class Warga {
    
    @Id @GeneratedValue
    @JoinTable(name = "surat_pengantar", joinColumns = @JoinColumn(name = "nik"),
            inverseJoinColumns = @JoinColumn(name = "nik"))
    private String nik;
    
    @Column(name="nama")
    private String nama;
    
    @Column(name="no_kk")
    private String no_kk;
    
    @Column(name = "umur")
    private String umur;
    
    @Column(name = "alamat")
    private String alamat;

    public Warga() {
    }

    public Warga(String nik, String nama, String no_kk, String umur, String alamat) {
        this.nik = nik;
        this.nama = nama;
        this.no_kk = no_kk;
        this.umur = umur;
        this.alamat = alamat;
    }
    
    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNo_kk() {
        return no_kk;
    }

    public void setNo_kk(String no_kk) {
        this.no_kk = no_kk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    
}
