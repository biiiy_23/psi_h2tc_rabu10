/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 *
 * @author HP
 */
@Entity
@Table(name="surat_pengantar")
public class SuratPengantar implements Serializable {
    
    @Id
    @Column(name="id_surat", unique = true)
    @GeneratedValue
    private int id_surat;
    
    @Column(name = "id_rw")
    private int id_rw;
    
    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_persyaratan")
    private Persyaratan persyaratan;
    
    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "nik")
    private Warga warga;
    
    @Column(name = "perihal")
    private String perihal;
    
    @Column(name = "tanggal_pembuatan")
    private Date tanggal_pembuatan;

    public SuratPengantar() {
    }

    public SuratPengantar(int id_surat, int id_rw, Persyaratan persyaratan, Warga warga, String perihal, Date tanggal_pembuatan) {
        this.id_surat = id_surat;
        this.id_rw = id_rw;
        this.persyaratan = persyaratan;
        this.warga = warga;
        this.perihal = perihal;
        this.tanggal_pembuatan = tanggal_pembuatan;
    }

    public int getId_surat() {
        return id_surat;
    }

    public void setId_surat(int id_surat) {
        this.id_surat = id_surat;
    }

    public int getId_rw() {
        return id_rw;
    }

    public void setId_rw(int id_rw) {
        this.id_rw = id_rw;
    }

    public Persyaratan getPersyaratan() {
        return persyaratan;
    }

    public void setPersyaratan(Persyaratan persyaratan) {
        this.persyaratan = persyaratan;
    }

    public Warga getWarga() {
        return warga;
    }

    public void setWarga(Warga warga) {
        this.warga = warga;
    }

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public Date getTanggal_pembuatan() {
        return tanggal_pembuatan;
    }

    public void setTanggal_pembuatan(Date tanggal_pembuatan) {
        this.tanggal_pembuatan = tanggal_pembuatan;
    }
}
