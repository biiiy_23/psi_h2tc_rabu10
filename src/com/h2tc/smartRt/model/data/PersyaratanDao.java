/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.data;

import com.h2tc.smartRt.model.pojo.Persyaratan;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author HP
 */
public class PersyaratanDao {
    SessionFactory sessionFactory = Hibernate.getSessionFactory();
    
    public Session openSession() {
        return sessionFactory.openSession();
    }
    
    public void save(Persyaratan persyaratan) {
        Session sess = this.openSession();
        Transaction t = sess.beginTransaction();
        sess.save(persyaratan);
        t.commit();
    }
    
    public void update(Persyaratan persyaratan) {
        Session sess = this.openSession();
        Transaction t = sess.beginTransaction();
        sess.update(persyaratan);
        t.commit();
    }
}
