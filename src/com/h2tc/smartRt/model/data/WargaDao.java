/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.data;

import com.h2tc.smartRt.model.pojo.Warga;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author HP
 */
public class WargaDao {
    SessionFactory sessionFactory = Hibernate.getSessionFactory();
    
    public Session openSession() {
        return sessionFactory.openSession();
    }
    
    public Warga getWargaByNik(String nik) {
        Session sess = this.openSession();
        return (Warga) sess.load(Warga.class, nik);
    }
    
    public void save(Warga warga) {
        Session sess = this.openSession();
        Transaction t = sess.beginTransaction();
        sess.save(warga);
        t.commit();
    }
}
