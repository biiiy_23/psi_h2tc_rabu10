/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.data;

import com.h2tc.smartRt.model.pojo.SuratPengantar;
import com.h2tc.smartRt.model.pojo.Warga;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author HP
 */
public class SuratPengantarDao {
    SessionFactory sessionFactory = Hibernate.getSessionFactory();
    
    public Session openSession() {
        return sessionFactory.openSession();
    }
    
    public List<SuratPengantar> getAll() {
        Session sess = this.openSession();
        Criteria c = sess.createCriteria(SuratPengantar.class).addOrder(Order.asc("id_surat"));
        List<SuratPengantar> suratPengantarList = c.list();
        return suratPengantarList;
    }

    public void save(SuratPengantar sp) {
        Session sess = this.openSession();
        Transaction t = sess.beginTransaction();
        sess.saveOrUpdate(sp);
        t.commit();
    }
}
