/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.model.data;

import com.h2tc.smartRt.model.pojo.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author HP
 */
public class UserDao {
    SessionFactory sessionFactory = Hibernate.getSessionFactory();
    
    public Session openSession() {
        return sessionFactory.openSession();
    }
    
    public User getUserById(String id) {
        Session sess = this.openSession();
        return (User) sess.load(User.class, id);
    }
}
