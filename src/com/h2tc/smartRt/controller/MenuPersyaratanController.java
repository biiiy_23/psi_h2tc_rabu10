/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.controller;

import com.h2tc.smartRt.model.data.PersyaratanDao;
import com.h2tc.smartRt.model.data.SuratPengantarDao;
import com.h2tc.smartRt.model.data.WargaDao;
import com.h2tc.smartRt.model.pojo.Persyaratan;
import com.h2tc.smartRt.model.pojo.SuratPengantar;
import com.h2tc.smartRt.model.pojo.Warga;
import com.h2tc.smartRt.view.MenuPersyaratanFrame;
import java.util.List;


/**
 *
 * @author HP
 */
public class MenuPersyaratanController {
    private WargaDao wDao;
    private PersyaratanDao pDao;
    private Warga warga;
    private Persyaratan persyaratan;
    private SuratPengantar suratPengantar;
    private MenuPersyaratanFrame mpFrame;

    public MenuPersyaratanController(WargaDao wDao, PersyaratanDao pDao, Warga warga, Persyaratan persyaratan, SuratPengantar suratPengantar, MenuPersyaratanFrame mpFrame) {
        this.wDao = wDao;
        this.pDao = pDao;
        this.warga = warga;
        this.persyaratan = persyaratan;
        this.suratPengantar = suratPengantar;
        this.mpFrame = mpFrame;
    }
    
    public void saveWarga(Warga warga) {
        wDao = new WargaDao();
        wDao.save(warga);
    }
    
    public boolean checkPersyaratan(Warga warga, Persyaratan persyaratan) {
        return !(warga.getUmur().isEmpty() ||warga.getNik().isEmpty() || warga.getNo_kk().isEmpty() ||warga.getNama().isEmpty()
                ||warga.getAlamat().isEmpty() ||persyaratan.getKk().isEmpty() ||persyaratan.getKtp().isEmpty());
    }
    
    public boolean checkName(Warga warga) {
        return warga.getNama().matches("[a-zA-Z]+");
    }
    
    public boolean checkLengthNik(Warga warga) {
        return warga.getNik().length() == 16;
    }
    
    public boolean checkValueNik(Warga warga) {
        return warga.getNik().matches("[0-9]+");
    }
    
    public boolean checkLengthNo_kk(Warga warga) {
        return warga.getNo_kk().length() == 16;
    }
    
    public boolean checkValueNo_KK(Warga warga) {
        return warga.getNo_kk().matches("[0-9]+");
    }
    
    public boolean checkValueUmur(Warga warga) {
        return warga.getUmur().matches("[0-9]+");
    }
    
    public boolean checkLengthUmur(Warga warga) {
        return warga.getUmur().length() == 2;
    }
}
