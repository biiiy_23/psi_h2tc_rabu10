/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.controller;

import com.h2tc.smartRt.model.data.UserDao;
import com.h2tc.smartRt.model.pojo.User;
import com.h2tc.smartRt.view.LoginForm;

/**
 *
 * @author HP
 */
public class LoginController {
    private User user;
    private LoginForm loginForm;
    private UserDao uDao;

    public LoginController(User user, LoginForm loginForm, UserDao uDao) {
        this.user = user;
        this.loginForm = loginForm;
        this.uDao = uDao;
    }
    
    public boolean doRtLogin() {
        uDao = new UserDao();
        User akun = uDao.getUserById(user.getUsername());
        
        return akun.getUserLevel().equals("1");
    }
    
    public boolean doRwLogin() {
        uDao = new UserDao();
        User akun = uDao.getUserById(user.getUsername());
        
        return akun.getUserLevel().equals("2");
    }
}
