/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.h2tc.smartRt.controller;

import com.h2tc.smartRt.model.data.SuratPengantarDao;
import com.h2tc.smartRt.model.pojo.Persyaratan;
import com.h2tc.smartRt.model.pojo.SuratPengantar;
import com.h2tc.smartRt.model.pojo.Warga;
import java.util.List;

/**
 *
 * @author HP
 */
public class MenuUtamaRwController {
    private SuratPengantarDao spDao;
    private Warga warga;
    private Persyaratan persyaratan;

    public MenuUtamaRwController(SuratPengantarDao spDao, Warga warga, Persyaratan persyaratan) {
        this.spDao = spDao;
        this.warga = warga;
        this.persyaratan = persyaratan;
    }
    
    public List<SuratPengantar> getAllSuratPengantar() {
        spDao = new SuratPengantarDao();
        return spDao.getAll();
    }
}
